# Cloudbox Aura Edition

> Self-Hosted Cloud powered by Docker + Ansible

## Installation

### Clone repo.
```sh
git clone https://gitlab.com/mprasanjith/Cloudbox.git ~/cloudbox
cd ~/cloudbox/scripts
chmod +x *.sh
```

### Install dependencies and Cloudbox
```sh
./init.sh
sudo ./deps.sh
```

### Setup Ansible Vault

```sh
export EDITOR=nano
nano ~/.ansible_vault
```
Type in your preferred Ansible vault password.

```sh
ansible-vault encrypt ~/cloudbox/accounts.yml
```

### Update configs

```sh
ansible-vault edit ~/cloudbox/accounts.yml
```

Update the configs in accounts.yml. A reference documentation for all supported features can be found [here](https://gitlab.com/mprasanjith/Cloudbox/-/wikis/accounts.yml-Configuration).

### Preinstall

```sh
sudo ansible-playbook cloudbox.yml --tags preinstall
```

### Install

```sh
sudo ansible-playbook cloudbox.yml --tags cloudbox
```
